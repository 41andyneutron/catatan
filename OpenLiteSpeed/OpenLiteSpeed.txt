##Installasi OpenLiteSpeed -> https://openlitespeed.org/kb/install-ols-from-litespeed-repositories/
	- $ sudo yum install epel-release
	- $ sudo rpm -Uvh http://rpms.litespeedtech.com/centos/litespeed-repo-1.1-1.el8.noarch.rpm
	- $ sudo yum install openlitespeed

==== Setelah Install OpenLiteSpeed
	- $ sudo service lsws {start|restart|stop|reload|condrestart|try-restart|status|help} 
	start       - start web server 
	stop        - stop web server 
	restart     - gracefully restart web server with zero down time 
	reload      - same as restart 
	condrestart - gracefully restart web server if server is running 
	try-restart - same as condrestart 
	status      - show service status 
	help        - this screen

	- $ sudo cat /usr/local/lsws/adminpasswd
	- Webadmin : https://localhost:7080
	- Example : http://localhost:8088
	- Reset the WebAdmin password from the command line: /usr/local/lsws/admin/misc/admpass.sh
